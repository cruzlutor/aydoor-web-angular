'use strict';

var gulp = require('gulp'),
    stylus = require('gulp-stylus'),
    connect = require('gulp-connect');

gulp.task('style', function(){
    gulp.src([ './assets/css/main.styl', ])
    .pipe(stylus())
    .pipe(gulp.dest('./assets/css/'))
    .pipe(connect.reload());
});