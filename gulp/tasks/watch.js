'use strict';

var gulp = require('gulp'),
    watch = require('gulp-watch'),
    connect = require('gulp-connect');

gulp.task('watch', function(){
    gulp.watch('./assets/css/**/*.styl', ['style']);
    gulp.watch('./assets/js/main.js', ['scripts']);
    gulp.watch(['./assets/views/**/*.html', './assets/*.html'], ['html']);
});
