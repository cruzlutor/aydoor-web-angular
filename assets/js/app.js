'use strict';

// requires
var angular 	= require('angular');


// load angular requires
require('angular-resource');
require('angular-route');
require('angular-ui-router');


angular.element(document).ready(function() {

	var requires = [
		'ngResource',
		'ngRoute',
		'ui.router',
	];

	// init app
	angular.module('app', requires);

	// load config
	angular.module('app').config(require('./config'));

	// load run
	angular.module('app').run(require('./run'));

	// manual bootstraping
	angular.bootstrap(document, ['app']);

});