'use strict';

// requires
module.exports = function($stateProvider, $locationProvider, $urlRouterProvider){

	$locationProvider.html5Mode(true);

    $stateProvider
        .state('index', {
            url:            '/',
            templateUrl:    '/views/index.html',
        })


    $urlRouterProvider.otherwise('/');    
}